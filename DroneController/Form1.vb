﻿Imports System.IO.Ports

Public Class Form1

    Dim throttle As Integer
    Dim shouldExit As Boolean
    Dim Serial As IO.Ports.SerialPort
    ReadOnly sensitivity As Single = 0.2

    Sub SendThrottle()
        Serial.WriteLine("t " & throttle)
    End Sub

    Sub GetSerialPorts() Handles UpdatePorts.Click
        ListBox1.Items.Clear()
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ListBox1.Items.Add(sp)
        Next
    End Sub

    Sub MainLoop()
        If Serial IsNot Nothing AndAlso Serial.IsOpen Then
            SendThrottle()
            AppConsole.ScrollToCaret()
        End If
    End Sub

    Sub Main() Handles MyBase.Load
        GetSerialPorts()
        Label2.Text = "0%"
        Timer1.Interval = 1000 / 10
        Timer1.Enabled = True
    End Sub

    Sub ChangeThrottleMouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseWheel
        SetThrottle(throttle + e.Delta * sensitivity, True)

    End Sub

    Sub ChangeThrottleTrackbar() Handles TrackBar1.ValueChanged
        SetThrottle(TrackBar1.Value, False)
    End Sub

    Sub SetThrottle(ByVal t As Single, ByVal updateTrackBar As Boolean)
        throttle = Math.Max(0, Math.Min(1000, t))
        Label2.Text = (throttle / 10) & "%"
        If updateTrackBar Then
            TrackBar1.Value = throttle
        End If
    End Sub

    Sub StopRunning() Handles MyBase.FormClosing
        shouldExit = True
        If Serial IsNot Nothing AndAlso Serial.IsOpen Then Serial.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        MainLoop()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Serial IsNot Nothing AndAlso Serial.IsOpen Then
            Serial.Close()
        End If
        Serial = New SerialPort(ListBox1.SelectedItem, 57600, Parity.None, 8, StopBits.One)
        Serial.Handshake = Handshake.None
        Serial.Encoding = System.Text.Encoding.Default
        Serial.Open()
        AppConsole.AppendText("Connected to " & Serial.PortName & vbCrLf)
        AppConsole.ScrollToCaret()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Serial IsNot Nothing AndAlso Serial.IsOpen Then
            Serial.Close()
            AppConsole.AppendText("Disconnected from " & Serial.PortName & vbCrLf)
        End If
    End Sub

    Private Sub ConsoleInput_KeyDown(sender As Object, e As KeyEventArgs) Handles ConsoleInput.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            If Serial IsNot Nothing AndAlso Serial.IsOpen Then
                Serial.WriteLine(ConsoleInput.Text)
                ConsoleInput.ResetText()
            End If
        End If
    End Sub
End Class
